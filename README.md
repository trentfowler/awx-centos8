1. Set up ansible on control node: 
    ```bash
    su - 
    
    dnf install -y epel-release
    dnf install -y ansible git

    useradd ansible
    echo "password" | passwd --stdin ansible
    echo "ansible ALL=(ALL)       NOPASSWD: ALL" > /etc/sudoers.d/ansible
    
    su - ansible

    ssh-keygen
    ssh-copy-id localhost
    ```
2. Install git and clone repo: 
    ```bash
    git clone https://gitlab.com/trentfowler/awx-centos8.git
    ```
3. Run setup playbook: 
    ```bash
    cd awx-centos8/
    ansible-playbook setup.yml

    # refresh user groups to use docker 
    su - ansible
    ```
4. Run install playbook:
    ```bash
    cd playbooks/awx-centos8/local/awx-*/installer
    ansible-playbook -i inventory install.yml
    ```
